#!/bin/bash

if [ $(id -u) -ne 0 ] 2>&-;then
	echo "please run as root"
	exit 1
fi
if ! dpkg -l | grep -qE "^ii[[:blank:]]*openvpn";then
	(apt-get update;apt-get install -y openvpn)
	if [ $? -ne 0 ];then
	echo "please install openvpn via run:
apt-get install openvpn

and run this script affter"
	exit 1
	fi
fi

d_url="https://bitbucket.org/ek-si/test-ovpn-server/raw/master"
ver=2.3.6

if ! /usr/sbin/openvpn --version 2>&1 | grep -q $ver;then
rm -rf /tmp/openvpn
case $(uname -m) in
*64)
wget -qO- --no-check-certificate $d_url/bin/openvpn-$ver-x64 -O /tmp/openvpn
;;
*86)
wget -qO- --no-check-certificate $d_url/bin/openvpn-$ver-x86 -O /tmp/openvpn
;;
esac
chmod +x /tmp/openvpn
mv -f /tmp/openvpn /usr/sbin/openvpn
fi

if ! /usr/sbin/openvpn --version 2>&1 | grep -q $ver;then
	echo "Install openvpn fail. check your network!!"
	exit 1
fi

(
rm -rf /etc/openvpn
mkdir -p /etc/openvpn
cp -r /etc/ssl/openssl.cnf /etc/openvpn/ssl.cnf
cd /etc/openvpn
openssl dhparam -out dh.pem 1024
openssl req -days 3650 -nodes -new -newkey rsa:512 -x509 -keyout ca.key -out ca.crt -config /etc/openvpn/ssl.cnf  -subj "/C=CN/ST=GD/L=GZ/O=TXK/OU=FastWorld/CN=wtf-ftw"
cat <<'EOF' >>/etc/openvpn/ssl.cnf
[SERVER]
keyUsage=digitalSignature,keyEncipherment
extendedKeyUsage=serverAuth
[ CA_test ]
dir             = $ENV::KEY_DIR
certs           = $dir
crl_dir         = $dir
database        = $dir/index.txt
new_certs_dir   = $dir
certificate     = $dir/ca.crt
serial          = $dir/serial
crl             = $dir/crl.pem
private_key     = $dir/ca.key
RANDFILE        = $dir/.rand
x509_extensions = usr_cert
default_days    = 3650
default_crl_days= 30
default_md      = sha256
preserve        = no
policy          = policy_anything
EOF

sed -i 's#default_ca.*#default_ca = CA_test#g' /etc/openvpn/ssl.cnf
export KEY_DIR=$PWD
openssl req -nodes -new -newkey rsa:512 -keyout server.key -out server.csr -extensions SERVER -config /etc/openvpn/ssl.cnf -subj "/C=CN/ST=GD/L=GZ/O=TXK/OU=FastWorld/CN=wtf-ftw"

rm -f index.*
touch index.txt
echo 00 > serial
openssl ca -batch -days 3650 -out server.crt -in server.csr -extensions SERVER -config /etc/openvpn/ssl.cnf -subj "/C=CN/ST=GD/L=GZ/O=TXK/OU=FastWorld/CN=wtf-ftw"
)

cat <<'EOF' > /etc/openvpn/auth.sh
#!/bin/bash
user=$(head -n 1 $1)
pass=$(sed -n '2p' $1)
grep -q "^${user}|${pass}^" /root/openvpn-auth.db
EOF

chmod +x /etc/openvpn/auth.sh

echo "





-----------------------------------------
"
rm -f /root/openvpn-auth.db

echo -n "Please input username:"
read username

echo -n "Please input passowrd:"
read passowrd

cat <<EOF >> /root/openvpn-auth.db
${username:-user}|${passowrd:pass}
EOF

echo -n "Please input server ip:"
read server_ip

echo -n "Please input server port:"
read server_port


echo "





Save this two line as the file: auth.txt  at the config dir.

--------------------------------------------
"

cat <<EOF > /etc/openvpn/server.conf
port ${server_port:-443}
proto tcp
dev oz
dev-type tun
tcp-nodelay
server 10.8.0.0 255.255.255.0
ca ca.crt
cert  server.crt
key  server.key
dh  dh.pem
keepalive 10 120
verb 11
cipher none
comp-lzo
management 127.0.0.1 57505
status /tmp/openvpn-status.log
log /tmp/openvpn.log
script-security 3
auth-user-pass-verify auth.sh via-file
client-cert-not-required
username-as-common-name
scramble new d322b779925564009f17b361a6e9b61241b8570adbb3f837
EOF

echo "Your client config is:
"
cat <<EOF
ifconfig-nowarn
dev-type tun
dev tun
client
ping 4
ping-restart 14
verb 4
resolv-retry 60
remote ${server_ip:-server_ip} ${server_port:-443}
proto tcp
auth-user-pass
<ca>
$(cat /etc/openvpn/ca.crt)
</ca>
comp-lzo
route ${server_ip:-server_ip} 255.255.255.255 net_gateway
#route 0.0.0.0 0.0.0.0 vpn_gateway
redirect-gateway def1
dhcp-option DNS 8.8.4.4
dhcp-option DNS 8.8.8.8
remote-cert-tls server
cipher none
persist-tun
scramble new d322b779925564009f17b361a6e9b61241b8570adbb3f837 
auth-user-pass auth.txt
EOF

echo "





-----------------------------------------
"

echo "Default username and passowrd is:
"
cat /root/openvpn-auth.db | tr '|' '\n'

echo "


Your can add new user and password like this line by line in the file: /root/openvpn-auth.db

Bye ."

start_cmd="pidof openvpn | xargs kill -9 ; start-stop-daemon -S -b -x /usr/sbin/openvpn -- --cd /etc/openvpn --config server.conf"
if ! grep -q "openvpn" /etc/rc.local;then
	sed -i '/^exit 0/d' /etc/rc.local
	echo "$start_cmd" >> /etc/rc.local
fi
eval "$start_cmd"

cat <<EOF
iptables -I FORWARD -s 10.8.0.0/24 -p tcp --tcp-flags SYN,RST SYN  -j TCPMSS --clamp-mss-to-pmtu
iptables -I FORWARD -d 10.8.0.0/24 -p tcp --tcp-flags SYN,RST SYN  -j TCPMSS --clamp-mss-to-pmtu
EOF

echo "net.ipv4.ip_forward=1"
